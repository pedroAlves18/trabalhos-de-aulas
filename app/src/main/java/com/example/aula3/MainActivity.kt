package com.example.aula3

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    val email: EditText by lazy{
        findViewById<EditText>(R.id.main_et_email)
    }
    val password: EditText by lazy {
        findViewById<EditText>(R.id.main_et_password)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val email = findViewById<EditText>(R.id.main_et_email);
        val password = findViewById<EditText>(R.id.main_et_password);
        Toast.makeText(this,email.text,Toast.LENGTH_LONG).show();*/

        val button = findViewById<Button>(R.id.main_btn_login)
        button.setOnClickListener{view ->
            if(isEmailValid(email.text.toString())) doLogin(view)
            else errorEmailMessage(view)
        }
    }
    fun doLogin(view: View) {
        //val email = findViewById<EditText>(R.id.main_et_email);
        Toast.makeText(this,email.text,Toast.LENGTH_LONG).show();
    }
    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun errorEmailMessage(view: View){
        return Toast.makeText(this,R.string.emailError,Toast.LENGTH_LONG).show();
    }
}
